# Setup environment for big data analytics II course
based on this course: [https://lms.hnee.de/course/view.php?id=1591](https://lms.hnee.de/course/view.php?id=1591)

## setup locally

```shell
conda create -n big_data_analytics_II python=3.10
conda activate big_data_analytics_II
pip install -r requirement.txt 
```

Test if everything is allright
```shell
python --version # should be 3.10. 
```

```shell
# start jupyter lab
jupyter lab
```

## Docker version
Install docker. If you are not sure what to install take Docker Desktop: [https://docs.docker.com/desktop](https://docs.docker.com/desktop)


### build on an arm based mac 
```shell
## arm m2 mac
docker-compose -f docker-compose.build.yml build base-python-arm64
```

### build on an amd or intel based windows/linux/mac
```shell
docker compose -f docker-compose.build.yml build base-python-amd64
```

### interact with the registry
```shell
## push the images to the registry
docker-compose -f docker-compose.build.yml push
## pull the images 
docker-compose -f docker-compose.build.yml pull
```

TODO, it should be easy to start the container using docker compose, but the port mapping doesn't work
### run the container
start the images locally
```shell
# Mac
docker run -it --rm -p 8888:8888 -v ${PWD}/notebooks:/notebooks registry.gitlab.com/hnee-courses/big_data_analytics_2/big_data_analytics_ii-base-python-arm64:latest

linux bash
docker run -it --rm -p 8888:8888 -v ${PWD}/notebooks:/notebooks registry.gitlab.com/hnee-courses/big_data_analytics_2/big_data_analytics_ii-base-python-amd64:latest

# Windows/linux
docker run -it --rm -p 8888:8888 -v ./notebooks:/notebooks registry.gitlab.com/hnee-courses/big_data_analytics_2/big_data_analytics_ii-base-python-amd64:latest

```


## setup a local cluster with worker and scheduler
```shell
docker compose -f docker-compose.cluster.yml up dask-scheduler dask-worker

# TODO broken docker compose -f docker-compose.cluster.yml up notebook

## shut it down again
docker compose -f docker-compose.cluster.yml down
```

move the docker-compose script to the machine. Then ssh into it and execute a docker compose up
```shell
scp docker-compose.cluster.yml christian@fb1ki.hnee.de:/tmp/
```

```shell
docker compose -f docker-compose.cluster.yml up dask-scheduler dask-worker
```

open the scheduler dashboard on [http://fb1ki.hnee.de:18787/status](http://fb1ki.hnee.de:18787/status)