# FROM ultralytics/yolov5:v7.0-arm64 AS app

ARG IMAGE_NAME
FROM $IMAGE_NAME AS builder
# FROM jupyter/scipy-notebook:2023-02-28 AS builder

ARG MINICONDA_ARCHITECTURE


RUN pip install --no-cache wheel
RUN pip install --no-cache --upgrade pip

# Install miniconda according to https://fabiorosado.dev/blog/install-conda-in-docker/
# Install base utilities
RUN apt-get update
RUN apt-get install -y build-essential wget git ffmpeg libsm6 libxext6
RUN apt-get clean
RUN rm -rf /var/lib/apt/lists/*

# Install miniconda
ENV CONDA_DIR /opt/conda
## python 3.10 from https://docs.conda.io/en/main/miniconda.html
RUN wget --quiet $MINICONDA_ARCHITECTURE -O ~/miniconda.sh && \
     /bin/bash ~/miniconda.sh -b -p /opt/conda

# Put conda in path so we can use conda activate
ENV PATH=$CONDA_DIR/bin:$PATH

RUN conda install gdal nb_conda_kernels ipykernel

COPY requirements.txt requirements.txt
RUN pip install --no-cache -r requirements.txt




ENV PATH="/opt/venv/bin:$PATH"

CMD ["jupyter", "lab", "--allow-root", "--ip=0.0.0.0", "--notebook-dir=/notebooks"]
