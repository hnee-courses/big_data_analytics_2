"""
slice images into smaller parts
"""
import os
from time import sleep

from loguru import logger



def slice_very_big_raster(base_path, image_name, x_size=None, y_size=None, FORCE_UPDATE=False, file_type="JPEG"):
    """
    slice the very big geotiff into smaller parts which can be handled by Yolo
    https://www.youtube.com/watch?v=H5uQ85VXttg

    :param base_path:
    :param image_name:
    :return:
    @param base_path:
    @param image_name:
    @param FORCE_UPDATE:
    @param y_size:
    @param x_size:
    """

    #     TODO implement the filetype we are not always slicing the raster twice
    #     TODO move this somewhere else

    logger.info(f"Slice the raster {image_name} into x:{x_size} by y:{y_size} sized tiles")

    from osgeo import gdal
    sliced_paths = []
    image_names = []

    dem_path = str(base_path.joinpath(image_name))
    os.path.getsize(dem_path)  ## gdal.Open is not realizing when the file is missing. So do this before
    dem = gdal.Open(dem_path)
    gt = dem.GetGeoTransform()

    # get coordinates of upper left corner
    xmin = gt[0]
    ymax = gt[3]
    res = gt[1]  # resolution, GSD

    # determine total length of raster
    xlen = res * dem.RasterXSize
    ylen = res * dem.RasterYSize

    if x_size is None and y_size is None:

        # number of tiles in x and y direction
        xdiv = 3
        ydiv = 3

    else:
        import math
        div_x_factor = math.ceil(dem.RasterXSize / x_size)
        div_y_factor = math.ceil(dem.RasterYSize / y_size)

        # number of tiles in x and y direction
        xdiv = div_x_factor
        ydiv = div_y_factor

    # size of a single tile
    xsize = xlen / xdiv
    ysize = ylen / ydiv

    # create lists of x and y coordinates
    # keep in mind the values are bases on the projections
    xsteps = [xmin + xsize * i for i in range(xdiv + 1)]
    ysteps = [ymax - ysize * i for i in range(ydiv + 1)]

    sliced_path = get_geospatial_sliced_path(base_path, x_size, y_size)

    sliced_path.mkdir(exist_ok=True)

    # loop over min and max x and y coordinates
    slice_dict = {}
    for i in range(xdiv):
        for j in range(ydiv):
            logger.info(f"xdiv_ydiv: {i}_{j}")
            xmin = xsteps[i]
            xmax = xsteps[i + 1]
            ymax = ysteps[j]
            ymin = ysteps[j + 1]

            sliced_tif_image_path = sliced_path.joinpath(f"dem_translate_{i}_{j}.tif")
            sliced_jpg_image_path = sliced_path.joinpath(f"dem_translate_{i}_{j}.jpg")

            logger.info(sliced_tif_image_path)

            if not os.path.exists(str(sliced_tif_image_path)) or FORCE_UPDATE:
                logger.info(f"write {str(sliced_tif_image_path)} to disk")

                with open(sliced_tif_image_path, 'w') as f:
                    return_value = gdal.Translate(str(sliced_tif_image_path),
                                              dem,
                                              projWin=(xmin, ymax, xmax, ymin),
                                              xRes=res,
                                              yRes=-res,
                                              )
                # Properly close the datasets to flush to disk
            ## TODO implement compression here?
            """
            this attempt was a failure because it breaks the slicing somehow.
                            translate_options = gdal.TranslateOptions(format='GTiff',
                                                          creationOptions=['COMPRESS=LZW']
                                                          )
                                                          
            This:
            translate_options = gdal.TranslateOptions(creationOptions=['COMPRESS=JPEG']
            leads to completely empty tiffs                                                          
            """
            ## For image recognition this is necesarry ## TODO there should be a more elegant ways


            sliced_paths.append(sliced_path)
            image_names.append(sliced_jpg_image_path)

            slice_dict[sliced_jpg_image_path] = sliced_tif_image_path

    dem = None
    sliced_tif_image_path = None

    # close the open dataset!!!
    dem = None
    logger.info(f"wait for a second to ensure all buffers are closed.")
    sleep(1)
    return sliced_path, image_names, slice_dict


def get_geospatial_sliced_path(base_path, x_size, y_size):
    if x_size is not None and y_size is not None:
        sliced_path = base_path.joinpath(f"sliced_{x_size}_{y_size}px")
    else:
        sliced_path = base_path.joinpath(f"sliced")
    return sliced_path
