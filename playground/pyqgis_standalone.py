"""
scribble to build qgis standalone application.

https://digital-geography.com/how-to-use-pyqgis-as-standalone-script-on-ubuntu/

Unfortunately there is no seperate python package which makes it nearly impossible to distribute the work.
/Applications/QGIS.app/Contents/Resources/python

building a plugin:
https://www.qgistutorials.com/en/docs/3/building_a_python_plugin.html
"""

from qgis.core import *

# Supply path to qgis install location
QgsApplication.setPrefixPath("/Applications/QGIS.app", True)

# Create a reference to the QgsApplication.  Setting the
# second argument to False disables the GUI.
qgs = QgsApplication([], False)

# Load providers
qgs.initQgis()

# Write your code here to load some layers, use processing
# algorithms, etc.

# Finally, exitQgis() is called to remove the
# provider and layer registries from memory
qgs.exitQgis()
