"""
take a geotiff and las of an area remove add the las as another channel as another channel to the geotiff

0. images to orthophoto
1. las to point cloud to dem
2. rasterio.merge.merge https://rasterio.readthedocs.io/en/stable/api/rasterio.merge.html to combine both tiff rasters
3. export only three bands


"""
from loguru import logger
import dask

RUN_DISTRIBUTED = True

try:

    if RUN_DISTRIBUTED:
        from dask.distributed import Client
        client = Client("fb1ki.hnee.de:18786")

    ## for a very strange reason this local cluster doesn't work from within scripts. Only in an interactive notebooks.
    # else:
    #     from dask.distributed import LocalCluster
    #
    #     client = LocalCluster()
    #     scheduler_address = client.scheduler_address


    @dask.delayed
    def inc(x):
       return x + 1

    @dask.delayed
    def add(x, y):
       return x + y

    a = inc(1)       # no work has happened yet
    b = inc(2)       # no work has happened yet
    c = add(a, b)    # no work has happened yet

    c = c.compute()  # This triggers all of the above computations

    logger.info(c)

except Exception as e:
    logger.error(e)

finally:
    client.close()
